create database mathi;
drop database mathi;
use mathi;
create table student(
id int primary key,
name varchar(10),
gpa decimal(3,2)

);
describe student;
drop table student;
alter table student add column department varchar(5);
alter table student drop column department;
insert into student values(1,"mathi",5.6),(2,"sakthi",3.5),(3,"surya",2.5),(4,"tamil",1.8);
select * from student;
insert into student(id,name)values(5,"raja");
select * from student;
create table employee(
id int primary key,
ename varchar(10),
job_desc varchar(10),
salary int
);
describe employee;
insert into employee values(1,"mathi","CEO",100000);
insert into employee values(2,"mass","CEO",100000);
insert into employee values(3,"tamil","MANAGER",200000);
insert into employee values(4,"sakthi","MANAGER",300000);
insert into employee values(5,"tamil","SALES",500000);
insert into employee values(6,"surya","SALES",600000);
insert into employee values(7,"raja","HR",800000);
insert into employee values(8,"kabali","HR",1200000);
insert into employee values(9,"mani","ENGINEER",400000);
insert into employee values(10,"deena","ENGINEER",900000);

select * from employee;
select * from employee where salary>100000;
select * from employee where job_desc="CEO";
select * from employee where salary>400000 and job_desc="SALES";














